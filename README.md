
# Building the Video Capturing container


## Introduction
This document guides the user to build and deploy the video capturing container.

## Pre-Requisites
- ARM Based System Ready Platform. \
  The following platforms have been tested on: 
    - Rockchip 3399ProD
    - Radxa Rock4B  
    - NXP i.MX 8M Plus

- USB Drive Flashed with ARM System-Ready EWAOL Image.



- Video Source: \
docker container can run either with a USB webcam or an MP4 video file.  
Choose the most appropriate one. 

    - Video.mp4 file: This is optional but helpful to enable the analysis on video file which helps to minimize the requirement of MIPI/CSI camera or USB camera.

  -  USB webcam: The demo also works with a USB camera as input source which performs real time inference.  



## Steps overview
- Clone the repository
- Building the docker container
- Running the docker container with usb webcam
- Running the docker container with MP4 video file
- Next steps

## Clone the repository
- Clone the gitlab repo using below command
```sh
$ git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```
## Building the docker container
- Change the working directory to Video capturing contianer folder 
```sh
$ cd <PROJECT_ROOT>
```
- Build the docker container
```sh
$ docker build -t <IMAGE_NAME>:<IMAGE_TAG> .
```
- Update IMAGE_NAME and IMAGE_TAG. These can be given according to user choice eg. `video_capturing_container:v1`

## Running the docker container with usb webcam
- Use the below command to run the docker container to take input from usb webcam, update the image name and tag accordingly
```sh
$ docker run -it --network=host --device=/dev/video0 <IMAGAE_NAME>:<IMAGE_TAG> ./mediamtx usb_webcam.yml 
```
## Running the docker container with MP4 video file
- Copy a video file inside the project folder
```sh
$ cp <PATH_TO_MP4_VIDEO_FILE> <ROOT_OF_PROJECT_FOLDER>/video.mp4
```
- Open the Dockerfile and add a COPY instruction to copy the video file to container and save the file.
```sh
COPY <PATH_TO_MP4_VIDEO_FILE> video.mp4
```
- Build the docker container
```sh
$ docker build -t rtsp-server:latest .
```
- Run the docker contaienr
```sh
$ docker run -it --network=host <IMAGAE_NAME>:<IMAGE_TAG> ./mediamtx mp4.yml 
``` 
This will start the RTSP streaming server at port number 8554/cam, the user can play the stream with below link
```sh
rtsp://<IP_ADDRESS_OF_DEVICE>:8554/cam
```

## Next steps
- Clone and build the inference container.
