# Use the aler9/rtsp-simple-server image as a base for the rtsp stage
FROM aler9/rtsp-simple-server AS rtsp

# Use Alpine Linux version 3.12 as the base image
FROM alpine:3.12

# Install FFmpeg without caching package index
RUN apk add --no-cache ffmpeg

# Copy the compiled files from the 'rtsp' stage to the root directory
COPY --from=rtsp /mediamtx /

# Copy the mp4.yml configuration file
COPY mp4.yml .

# Copy the usb_webcam.yml configuration file
COPY usb_webcam.yml .

# COPY video.mp4 /video.mp4
